{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE UndecidableInstances       #-}

module Lib where

import qualified Data.AttoLisp as L
import Data.AttoLisp (Lisp(..))
import Data.ByteString.Lazy.Char8 as BS
import Data.Text (Text)


-- import           Control.Applicative
-- import           Control.Concurrent
-- import           Control.DeepSeq
-- import           Control.Monad.Trans
-- import           Control.Monad.Trans.Reader
-- import           Data.AttoLisp
-- import qualified Data.ByteString.Lazy.Char8 as B hiding (length)
-- import qualified Data.ByteString.Lazy.UTF8  as B (length)
-- import           Data.Monoid                ((<>))

-- class ToEmacs a where
--   toEmacs :: a -> Either (Emacs Lisp) Lisp

-- instance ToLisp a => ToEmacs a where
--   toEmacs = Right . toLisp

-- instance {-# OVERLAPS #-} ToLisp a => ToEmacs (Emacs a) where
--   toEmacs = Left . fmap toLisp


-- instance NFData (Emacs Lisp) where
--   rnf (EmacsInternal _) = ()

-- data Buffer = Buffer {text :: Text, point :: Int}

-- modifyBuffer :: (Buffer -> Buffer) -> Emacs ()
-- modifyBuffer f = getBuffer >>= putBuffer . f


-- putBuffer :: Buffer -> Emacs ()
-- putBuffer (Buffer t p) = eval_
--   [ Symbol "list"
--   , List [ Symbol "delete-region"
--          , List [ Symbol "point-min" ]
--          , List [ Symbol "point-max" ]]
--   , List [ Symbol "insert", String t ]
--   , List [ Symbol "goto-char"
--          , List [ Symbol "+"
--                 , Number . fromIntegral $ p-1
--                 , List [ Symbol "point-min" ]]]]

-- eval :: (ToLisp a, FromLisp a) => [Lisp] -> Emacs a
-- eval lsp = EmacsInternal $ do
--   (mvar, chan) <- ask
--   liftIO $ writeChan chan cmd
--   List (a:_) <- liftIO $ takeMVar mvar
--   case fromLisp a of
--        Success b -> return b
--        Error msg -> error msg
--   where cmd = let x = encode $ List [ Symbol "process-send-string"
--                                     , Symbol "haskell-emacs--proc"
--                                     , List [ Symbol "format"
--                                            , String "|%S"
--                                            , List [ Symbol "haskell-emacs--no-properties"
--                                                   , List [ Symbol "list"
--                                                          , List lsp]]]]
--               in encode [B.length x] <> x

-- eval_ :: [Lisp] -> Emacs ()
-- eval_ lsp = EmacsInternal $ do
--   (_, chan) <- ask
--   liftIO $ writeChan chan cmd
--   where cmd = let x = encode $ List lsp
--               in encode [B.length x] <> x


-- module Main where

-- import Lib
-- import qualified Data.AttoLisp as L
-- import Data.ByteString.Lazy.Char8 as BS


-- newtype Emacs a = EmacsInternal
--   {fromEmacs :: ReaderT (MVar Lisp, Chan B.ByteString) IO a}
--   deriving ( Functor
--            , Applicative
--            , Monad
--            , MonadIO
--            )

data Buffer = Buffer {text :: Text, point :: Int}

getBuffer :: Lisp
getBuffer =  List [ Symbol "list"
                  , List [ Symbol "buffer-string" ]
                  , List [ Symbol "point" ]
                  , List [ Symbol "point-min" ]]

-- def :: [] -> [Lisp] -> [Lisp]
-- def args

--   defunA "face" [] $ body $ do
--     message "Hello there!"


-- userConfig :: [Lisp]
-- userConfig =
--   [ [ ]

--   ]

-- def :: [] -> [Lisp] -> [Lisp]
-- def args

--   defunA "face" [] $ body $ do
--     message "Hello there!"


-- data EmacsLisp a = EmacsLisp Lisp a

defunA :: Text -> [Text] -> [Lisp] -> Lisp
defunA name args body =
  let
    args' = Symbol <$> args
  in
    List $ [ Symbol "defun"
           , Symbol name
           , List args'
           ] <> body

data ESymbol = ESymbol Text

data FnDef = FnDef
  { fnName :: ESymbol
  , fnArgs :: [ESymbol]
  , fnBody :: [ELisp]
  }

builtinFn0 :: String -> Function0 a
builtinFn0 = Function0


data ELisp
  = ESym ESymbol
  | EList [ELisp]
  | EDot
  | EStr String
  | ENum EmNum
  | Raw Lisp

newtype EmNum = EmNum String
  deriving (Show)

emNum :: Show a => Num a => a -> EmNum
emNum num = EmNum $ show num

fnquote2 :: (a -> b -> c) -> Function2 a b c
fnquote2 fn = undefined

data Function0 a = Function0 String

emacsFunction0 :: String -> Function0 a
emacsFunction0 name = Function0 name

data Function1 a b = Function1 String

emacsFunction1 :: String -> Function1 a b
emacsFunction1 name = Function1 name

message :: Function1 String ()
message = emacsFunction1 "message"

data Function2 a b c = Function2 String

emacsFunction2 :: String -> Function2 a b c
emacsFunction2 name = Function2 name

plus :: Function2 EmNum EmNum EmNum
plus = emacsFunction2 "+"

toString :: Lisp -> String
toString = unpack . L.encode . L.toLisp




-- data EFunction a b = EFunction Fn a b
-- function2 :: String -> EFunction a (EFunction b c)
-- function2 = undefined
