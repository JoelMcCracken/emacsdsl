{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE NoImplicitPrelude #-}

module Lib2 where

import Data.Monoid

import qualified Prelude as P
import Prelude (String, ($))

------------------------ Standard lib stuff

data EString = EString String

data EFunction i o
  = ENativeFn String
  | ELambda ESym (EBlock o)

data ESym = ESym String

data EVar a = EVar ESym a

data EBlock o
  = forall a . EBlock (EExpr o) (EBlock a)
  | EBlockNil

-- EBlock a >>| EBlock =

data EExpr o
  = EExprAppl o
  | EExprVar (EVar o)
  -- | EBlock (ECode o)
data ENil = ENil

data EAppl b = forall a . ToLisp a => EAppl (EFunction a b) a



------------------------ Conversion to lisp code

class ToLisp a where
  toLisp :: a -> String

instance ToLisp EString where
  toLisp (EString str) = "\"" <> str <> "\""

instance ToLisp ENil where
  toLisp ENil = "nil"

instance ToLisp (EFunction a b) where
  toLisp (ENativeFn name) = name
  toLisp (ELambda (ESym v) (EBlock (ESym v2) EBlockNil)) =
    "(lambda (" <> v <> ") " <> v2 <> ")"

instance ToLisp a => ToLisp (EAppl a) where
  toLisp (EAppl fn arg) =
    "(" <> toLisp fn <> " " <> toLisp arg <> ")"

-- >|> :: EExpr a -> EExpr b -> EExpr b
-- ()>|>



------------------------ Standard lib stuff

message :: EFunction EString ENil
message = ENativeFn "message"

-- ok, so if i have an explicit type for this, i need to somehow
invoked :: EAppl ENil
invoked = EAppl message $ EString "hello world"

eId :: EFunction i i
eId = ELambda (ESym "x") (EBlock (EExpr (EVar (ESym "x"))) EBlockNil)
