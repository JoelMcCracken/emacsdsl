{-# LANGUAGE OverloadedStrings          #-}

import Test.Hspec
import qualified Lib as L
-- import ib ((+))
import Lib (emNum)
import Data.AttoLisp (Lisp(..))

import Lib2

-- toEmacsLisp :: EmacsExpr -> String
-- toEmacsLisp = undefined


-- type EString = String

-- type ENum a = Num a => a

-- type EArgsList = [ESymbol]

-- type ESymbol = String

-- data EFn = EFn { efName :: String, efArgs :: EArgsList }

-- myprog = do
--   toEmacsLisp $ do
--     defunA "face" [] $ body $ do
--       message "Hello there!"

-- faceFn ::  Emacs ()
-- facefn =
--   defunA "face" [] $ body $ do
--     message "Hello there!"


main :: IO ()
main = hspec $ do
  describe "Emacs DSL" $ do
    it "outputs code for getBuffer" $ do
      let actual = L.toString L.getBuffer
          expected = "(list (buffer-string) (point) (point-min))"
      actual `shouldBe` expected
    it "defunA with empty body" $ do
      let fndef = L.defunA "face" [] body
          body = []
      -- TODO this should really be () not nil
      L.toString fndef `shouldBe` "(defun face nil)"
    it "compiles a more reasonable example" $ do
      let fndef = L.defunA "1+" [ "x" ] body
          body = [ List [ Symbol "+"
                        , Number 1
                        , Symbol "x" ]
                 ]
      L.toString fndef `shouldBe` "(defun 1+ (x) (+ 1 x))"

    it "will represent a number" $ do
      let x = emNum 27
      putStrLn $ show x

    it "lets me do addition" $ do
      let x = emNum 37
      let y = emNum 10
      -- let z = x + y
      putStrLn $ show y

  describe "Another round" $ do
    it "does a thing" $ do
      toLisp (EString "hi") `shouldBe` "\"hi\""
      toLisp (EAppl message $ EString "hi") `shouldBe` "(message \"hi\")"
      toLisp (EAppl eId $ EString "hi") `shouldBe` "((lambda (x) x) \"hi\")"
      toLisp (EAppl (ELambda (ESym "x")
                      (ECode (EAppl message (EString "x"))))
               ENil) `shouldBe` "((lambda (x) (message x)) nil)"
      toLisp ENil `shouldBe` "nil"
